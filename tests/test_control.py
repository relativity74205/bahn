from datetime import datetime

import pytest

from Control import Control, GC, db
from Timetable import Timetable
import config

datetime1 = datetime(2019, 1, 1, 12, 0, 0)
datetime2 = datetime(2019, 1, 1, 11, 0, 0)


def test_control_class(mocker):
    init_last_station_update_mocked = mocker.patch('control_functions.init_last_station_update')
    control = Control()

    assert isinstance(control.timetable, Timetable)
    assert control.stations == config.BAHNHOF_DICT.keys()
    assert control.seconds_between_updates
    assert control.sleep_time_between_loops
    assert control.max_default_plans
    assert control.last_station_updates

    init_last_station_update_mocked.assert_called_with(db, config.BAHNHOF_DICT.keys())


def test_main_loop(mocker):
    def inner_loop_mocked_side_effect():
        GC.flag_running = False

    _ = mocker.patch('control_functions.init_last_station_update')
    inner_loop_mocked = mocker.patch('Control.Control.main_inner_loop')
    inner_loop_mocked.side_effect = inner_loop_mocked_side_effect

    control = Control()
    control.sleep_time_between_loops = 0.01
    control.main_loop()
    inner_loop_mocked.assert_called_once()


def test_main_inner_loop(mocker):
    i = 0

    def get_next_update_type_mocked_side_effect(*args):
        nonlocal i
        if i == 0:
            i += 1
            return 'default', -1
        elif i == 1:
            i += 1
            return 'full', -1
        elif i == 2:
            i += 1
            return 'recent', 0.1

    get_next_update_type_mocked = mocker.patch('control_functions.get_next_update_type')
    get_next_update_type_mocked.side_effect = get_next_update_type_mocked_side_effect
    get_default_tables_mocked = mocker.patch('Control.Control.get_default_table')
    get_update_mocked = mocker.patch('Control.Control.get_update')
    init_last_station_update_mocked = mocker.patch('control_functions.init_last_station_update')
    sleep_mocked = mocker.patch('control_functions.sleep')

    stations = ['a', 'b', 'c']
    d = {}
    for station in stations:
        d[station] = {'default': None,
                      'full': None,
                      'recent': None}
    init_last_station_update_mocked.return_value = d

    control = Control()
    control.stations = stations
    control.main_inner_loop()

    assert len(get_default_tables_mocked.call_args_list) == 4
    assert len(get_next_update_type_mocked.call_args_list) == 3
    assert len(get_update_mocked.call_args_list) == 2
    sleep_mocked.assert_called_with(1.0)
    assert control.last_station_updates['a']['default']
    assert not control.last_station_updates['a']['full']
    assert not control.last_station_updates['a']['recent']
    assert not control.last_station_updates['b']['default']
    assert control.last_station_updates['b']['full']
    assert control.last_station_updates['b']['recent']
    assert not control.last_station_updates['c']['default']
    assert not control.last_station_updates['c']['full']
    assert control.last_station_updates['c']['recent']


@pytest.mark.parametrize('station, current_time, datehour, last_updates', [
    ('a', datetime2, (2019, 1, 1, 12), {'default': datetime1})
])
def test_get_default_table(mocker, control_mocked, station, current_time, datehour, last_updates):
    save_data_mocked = mocker.patch('control_functions.save_data')
    timetable_mocked = mocker.patch.object(control_mocked, 'timetable')
    trainstop_mocked = mocker.patch('TrainStop.TrainStop')
    trainstops = [trainstop_mocked(), trainstop_mocked()]
    timetable_mocked.get_default_timetable.return_value = trainstops
    control_mocked.get_default_table(station, current_time, datehour, last_updates)
    save_data_mocked.assert_called_with(db, trainstops, 'TrainStop')
    timetable_mocked.get_default_timetable.assert_called_with(station, current_time, *datehour)


@pytest.mark.parametrize('station, max_default_plans, current_time, last_update', [
    ('a', 1, datetime1, {'default': datetime2})
])
def test_get_default_tables(mocker, control_mocked, station, max_default_plans, current_time, last_update):
    missing_datehours = [(2019, 1, 1, 12), (2019, 1, 1, 13)]
    get_missing_default_plan_dates_mocked = \
        mocker.patch('timetable_date_functions.get_missing_default_plan_dates')
    get_missing_default_plan_dates_mocked.return_value = missing_datehours
    sleep_mocked = mocker.patch('control_functions.sleep')

    get_default_table_mocked = mocker.patch('Control.Control.get_default_table')
    control_mocked.get_default_tables(station, max_default_plans, current_time, last_update)
    get_missing_default_plan_dates_mocked.assert_called_with(current_time, last_update['default'], max_default_plans)
    sleep_mocked.assert_called_with(1.0)
    assert len(get_default_table_mocked.call_args_list) == 2
    assert get_default_table_mocked.call_args_list[0][0][2] == missing_datehours[0]
    assert get_default_table_mocked.call_args_list[1][0][2] == missing_datehours[1]


@pytest.mark.parametrize('station, update_type, current_time, last_updates', [
    ('a', 'full', datetime2, {'default': datetime1})
])
def test_get_update(mocker, control_mocked, station, update_type, current_time, last_updates):
    save_data_mocked = mocker.patch('control_functions.save_data')
    timetable_mocked = mocker.patch.object(control_mocked, 'timetable')
    trainstopchange_mocked = mocker.patch('TrainStopChange.TrainStopChange')
    trainstopchanges = [trainstopchange_mocked(), trainstopchange_mocked()]
    timetable_mocked.get_changes.return_value = trainstopchanges
    control_mocked.get_update(station, update_type, current_time, last_updates)
    save_data_mocked.assert_called_with(db, trainstopchanges, 'TrainStopChange')
    timetable_mocked.get_changes.assert_called_with(station, update_type, current_time)
