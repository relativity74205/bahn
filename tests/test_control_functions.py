from datetime import datetime
import unittest.mock as mock

import pytest

import control_functions as cf
from DatabaseConnection import DatabaseConnection


@pytest.mark.parametrize('stations, last_single_update', [
    ([], {}),
    (['c'], {'c': {'default': None,
                   'full': None,
                   'recent': None}
             }),
    (['a', 'b'], {'a': {'default': datetime(2019, 1, 1, 10, 0, 0),
                        'full': datetime(2019, 1, 1, 12, 10, 0),
                        'recent': datetime(2019, 1, 1, 12, 20, 0)},
                  'b': {'default': datetime(2019, 1, 1, 11, 0, 0),
                        'full': datetime(2019, 1, 1, 12, 30, 0),
                        'recent': datetime(2019, 1, 1, 12, 40, 0)}
                  }),
    (['a', 'b', 'c'], {'a': {'default': datetime(2019, 1, 1, 10, 0, 0),
                             'full': datetime(2019, 1, 1, 12, 10, 0),
                             'recent': datetime(2019, 1, 1, 12, 20, 0)},
                       'b': {'default': datetime(2019, 1, 1, 11, 0, 0),
                             'full': datetime(2019, 1, 1, 12, 30, 0),
                             'recent': datetime(2019, 1, 1, 12, 40, 0)},
                       'c': {'default': None,
                             'full': None,
                             'recent': None}
                       })
])
def test_init_last_station_update(stations, last_single_update, mocker):
    def get_last_tstamp_request_side_effect(*args, **kwargs):
        if kwargs['station'] == 'a' and kwargs['request_type'] == 'full':
            return datetime(2019, 1, 1, 12, 10, 0)
        elif kwargs['station'] == 'a' and kwargs['request_type'] == 'recent':
            return datetime(2019, 1, 1, 12, 20, 0)
        elif kwargs['station'] == 'b' and kwargs['request_type'] == 'full':
            return datetime(2019, 1, 1, 12, 30, 0)
        elif kwargs['station'] == 'b' and kwargs['request_type'] == 'recent':
            return datetime(2019, 1, 1, 12, 40, 0)
        else:
            return None

    def get_last_datehour_default_plan_mocked_side_effect(*args, **kwargs):
        if kwargs['station'] == 'a':
            return '2019010110'
        elif kwargs['station'] == 'b':
            return '2019010111'
        else:
            return None

    get_last_tstamp_request_mocked = mocker.patch('DatabaseConnection.DatabaseConnection.get_last_tstamp_request')
    get_last_tstamp_request_mocked.side_effect = get_last_tstamp_request_side_effect
    get_last_datehour_default_plan_mocked = mocker.patch('DatabaseConnection.DatabaseConnection.get_last_datehour_default_plan')
    get_last_datehour_default_plan_mocked.side_effect = get_last_datehour_default_plan_mocked_side_effect

    db = DatabaseConnection()
    last_single_updates = cf.init_last_station_update(db, stations)
    assert last_single_updates == last_single_update


@pytest.mark.parametrize('sleep_time, sleep_called', [
    (1, True),
    (1, True),
    (-1, False),
])
def test_sleep(sleep_time, sleep_called):
    with mock.patch('time.sleep') as sleep_mocked:
        cf.sleep(sleep_time)
        if sleep_called:
            sleep_mocked.assert_called_with(sleep_time)
        else:
            sleep_mocked.assert_not_called()


@pytest.mark.parametrize('objects, obj_name, save_bulk_called', [
    (None, 'a', False),
    ([], 'a', False),
    ([1], 'a', True),
])
def test_save_data(db, objects, obj_name, save_bulk_called):
    with mock.patch('DatabaseConnection.DatabaseConnection.save_bulk') as db_mocked:
        cf.save_data(db, objects, obj_name)
        if save_bulk_called:
            db_mocked.assert_called_with(objects, obj_name)
        else:
            db_mocked.assert_not_called()


@pytest.mark.parametrize('d1, d2, hours', [
    (datetime(2019, 1, 1, 13, 0, 0), datetime(2019, 1, 1, 12, 0, 0), 1.0),
    (datetime(2019, 1, 1, 12, 0, 0), datetime(2019, 1, 1, 12, 0, 0), 0.0)
])
def test_get_minutes_between_datetimes(d1, d2, hours):
    assert cf.get_minutes_between_datetimes(d1, d2) == hours * 60 * 60


@pytest.mark.parametrize('last_complete_update, update_type', [
    ({'default': None,
      'full': None,
      'recent': None}, ('default', -1)),
    ({'default': datetime(2019, 1, 1, 10, 0, 0),
      'full': datetime(2019, 1, 1, 10, 0, 0),
      'recent': datetime(2019, 1, 1, 10, 0, 0)}, ('default', -1)),
    ({'default': datetime(2019, 1, 1, 10, 0, 0),
      'full': None,
      'recent': None}, ('default', -1)),
    ({'default': datetime(2019, 1, 1, 12, 0, 0),
      'full': None,
      'recent': None}, ('default', -1)),
    ({'default': datetime(2019, 1, 1, 14, 0, 0),
      'full': None,
      'recent': None}, ('full', -1)),
    ({'default': datetime(2019, 1, 1, 14, 0, 0),
      'full': datetime(2019, 1, 1, 11, 0, 0),
      'recent': datetime(2019, 1, 1, 11, 0, 0)}, ('full', -1)),
    ({'default': datetime(2019, 1, 1, 14, 0, 0),
      'full': datetime(2019, 1, 1, 11, 0, 0),
      'recent': None}, ('full', -1)),
    ({'default': datetime(2019, 1, 1, 14, 0, 0),
      'full': datetime(2019, 1, 1, 12, 0, 0),
      'recent': datetime(2019, 1, 1, 12, 0, 0)}, ('recent', -1)),
    ({'default': datetime(2019, 1, 1, 14, 0, 0),
      'full': datetime(2019, 1, 1, 12, 0, 0),
      'recent': None}, ('recent', -1)),
    ({'default': datetime(2019, 1, 1, 14, 0, 0),
      'full': datetime(2019, 1, 1, 12, 0, 0),
      'recent': datetime(2019, 1, 1, 11, 59, 0)}, ('recent', 60)),
])
def test_get_next_update_type(last_complete_update, update_type):
    current_time = datetime(2019, 1, 1, 12, 0, 0)
    seconds_between_updates = {'default': 60 * 60,
                               'full': 30 * 60,
                               'recent': 120}

    assert cf.get_next_update_type(current_time, last_complete_update, seconds_between_updates) == update_type


@pytest.mark.parametrize('current_time, tstamp_last_update, recency_lifetime, delta, sleep_time', [
    (datetime(2019, 1, 1, 12, 0, 0), datetime(2019, 1, 1, 11, 58, 10), 120, 110, 10),
    (datetime(2019, 1, 1, 12, 0, 0), datetime(2019, 1, 1, 11, 58, 10), 60, -50, -1),
    (datetime(2019, 1, 1, 12, 0, 0), datetime(2019, 1, 1, 11, 58, 8), 120, 108, 12),
    (datetime(2019, 1, 1, 12, 0, 0), None, 120, -1, -1)
])
def test_get_sleep_time(current_time, tstamp_last_update, recency_lifetime, sleep_time, delta, mocker):
    time_since_last_update_mocked = mocker.patch('control_functions.time_since_last_update')
    time_since_last_update_mocked.return_value = delta
    assert cf.get_sleep_time(current_time, tstamp_last_update, recency_lifetime) == sleep_time
    time_since_last_update_mocked.assert_called_with(current_time, tstamp_last_update)


@pytest.mark.parametrize('current_time, tstamp_last_update, time_since_last_update', [
    (datetime(2019, 1, 1, 12, 0, 0), None, -1),
    (datetime(2019, 1, 1, 12, 0, 0), datetime(2019, 1, 1, 11, 0, 0), 3600),
])
def test_time_since_last_update(current_time, tstamp_last_update, time_since_last_update):
    assert cf.time_since_last_update(current_time, tstamp_last_update) == time_since_last_update
