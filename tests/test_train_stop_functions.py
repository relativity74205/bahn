from datetime import datetime

import pytest

import config
import train_stop_functions as tsf


@pytest.mark.parametrize('raw_event, line', [
    ({'tl': {'@c': 'ICE', '@n': '100'}, 'ar': {'p': '2'}, 'dp': {'p': '2'}}, 'ICE100'),
    ({'tl': {'@c': 'S', '@n': '1234'}, 'ar': {'p': '2', '@l': '2'}}, 'S2'),
    ({'tl': {'@c': 'S', '@n': '1234'}, 'dp': {'p': '2', '@l': '2'}}, 'S2'),
    ({'tl': {'@c': 'S', '@n': '1234'}, 'ar': {'p': '2', '@l': 2}}, 'S2'),
    ({'tl': {'@c': 'ABR', '@n': '1234'}, 'ar': {'p': '2', '@l': 'RE19'}}, 'RE19')
])
def test_get_line(raw_event, line):
    raw_event_keys = list(raw_event.keys())
    bahn_event_keys = config.BAHN_EVENT_KEYS
    train_type = tsf.get_value(bahn_event_keys, raw_event, 'train_type')
    train_number = tsf.get_value(bahn_event_keys, raw_event, 'train_number')
    assert tsf.get_line(bahn_event_keys, raw_event, raw_event_keys, train_type, train_number) == line


@pytest.mark.parametrize('ppth, expected', [
    ('a|b|c', 'a'),
    ('', None),
    (None, None),
    ('aa', 'aa')
])
def test_get_departure_place(ppth, expected):
    assert tsf.get_departure_place(ppth, {}) == expected


@pytest.mark.parametrize('ppth, expected', [
    ('a|b|c', 'c'),
    ('', None),
    (None, None),
    ('aa', 'aa')
])
def test_get_destination_place(ppth, expected):
    assert tsf.get_destination_place(ppth, {}) == expected


@pytest.mark.parametrize('pt, tstamp', [
    ('1912231234', datetime.strptime('2019-12-23 12:34:00', '%Y-%m-%d %H:%M:%S')),
    ('', None),
    (None, None)
])
def test_get_datetime(pt, tstamp):
    assert tsf.get_datetime(pt, {}) == tstamp


@pytest.mark.parametrize('raw_event, trainstop_id', [
    ({'@id': 'a', 'tl': {'@f': 'S'}}, 'a'),
    ({'tl': {'@f': 'S'}}, None),
    ({}, None),
    (None, None),
])
def test_get_id(raw_event, trainstop_id):
    bahn_event_keys = config.BAHN_EVENT_KEYS
    assert tsf.get_id(bahn_event_keys, raw_event) == trainstop_id


@pytest.mark.parametrize('date, hour, datehour', [
    ('20190101', 19, '2019010119'),
    ('20190101', '19', '2019010119'),
    ('20190101', 2, '2019010102'),
    ('20190101', '2', '2019010102'),
    ('20190101', '100', '20190101100'),
])
def test_get_datehour_request(date, hour, datehour):
    assert tsf.get_datehour_request(date, hour) == datehour


# noinspection PyTypeChecker
class TestGetRawEventKeys:
    @pytest.mark.parametrize('raw_event, raw_event_keys', [
        ({'a': 1}, ['a']),
        ({'a': 1, 'b': 1}, ['a', 'b']),
        ({}, []),
    ])
    def test_base_case(self, raw_event, raw_event_keys):
        assert tsf.get_raw_event_keys(raw_event) == raw_event_keys

    def test_bad_case(self):
        with pytest.raises(AttributeError):
            tsf.get_raw_event_keys('')


@pytest.mark.parametrize('raw_event, key, value', [
    ({'tl': {'@t': 'tt1', '@o': 'to1'}}, 'trip_type', 'tt1'),
    ({'tl': {'@t': 'tt1', '@o': 'to1'}}, 'owner', 'to1'),
    ({'tl': {'@t': 'tt1', '@o': 'to1'}}, 'filter_flags', None),
    ({}, 'trip_type', None),
    ('', 'trip_type', None),
])
def test_get_value(raw_event, key, value):
    bahn_event_keys = {'trip_type': {'para1': 'tl',
                                     'para2': '@t'},
                       'filter_flags': {'para1': 'tl',
                                        'para2': '@f'},
                       'owner': {'para1': 'tl',
                                 'para2': '@o'},
                       'train_type': {'para1': 'tl',
                                      'para2': '@c'},
                       'train_number': {'para1': 'tl',
                                        'para2': '@n'}
                       }
    assert tsf.get_value(bahn_event_keys, raw_event, key) == value
