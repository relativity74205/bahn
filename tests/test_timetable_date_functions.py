import pytest
from datetime import datetime

import timetable_date_functions as tdf


@pytest.mark.parametrize("last_datehour, last_time", [
    ('2019010101', datetime(2019, 1, 1, 1)),
    ('2019010100', datetime(2019, 1, 1, 0)),
    ('2019111', datetime(2019, 1, 1, 1)),
    (None, None),
    ('20', None)
])
def test_datehour_to_datetime(last_datehour, last_time):
    assert tdf.datehour_to_datetime(last_datehour) == last_time


@pytest.mark.parametrize("last_datehour, last_time", [
    (datetime(2019, 1, 1, 1), '2019010101'),
    (datetime(2019, 1, 1, 0), '2019010100'),
    (None, None),
    (datetime(2019, 1, 1), '2019010100')
])
def test_datetime_to_datehour(last_datehour, last_time):
    assert tdf.datetime_to_datehour(last_datehour) == last_time


@pytest.mark.parametrize("last_time, amount_missing_dates, missing_dates", [
    (datetime(2019, 1, 1, 1), 1, [(2019, 1, 1, 2)]),
    (datetime(2019, 1, 1, 1), 3, [(2019, 1, 1, 2),
                                  (2019, 1, 1, 3),
                                  (2019, 1, 1, 4)]),
    (datetime(2019, 1, 1, 1), 0, [])
])
def test_generate_missing_dates(last_time, amount_missing_dates, missing_dates):
    assert tdf.generate_missing_dates(last_time, amount_missing_dates) == missing_dates


@pytest.mark.parametrize("missing_dates, missing_dates_tuple", [
    ([datetime(2019, 1, 1, 1)], [(2019, 1, 1, 1)]),
    ([datetime(2019, 1, 1, 1), datetime(2019, 1, 1, 2), datetime(2019, 1, 1, 3)],
     [(2019, 1, 1, 1), (2019, 1, 1, 2), (2019, 1, 1, 3)]),
    ([], [])
])
def test_convert_missing_dates(missing_dates, missing_dates_tuple):
    assert tdf.convert_missing_dates(missing_dates) == missing_dates_tuple


@pytest.mark.parametrize("current_time, last_datehour, max_default_plans, missing_dates", [
    (datetime(2019, 3, 15, 18, 12, 12), datetime(2019, 3, 15, 18), 12, tdf.generate_missing_dates(datetime(2019, 3, 15, 19), 11)),
    (datetime(2019, 3, 15, 18, 12, 12), datetime(2019, 3, 15, 19), 12, tdf.generate_missing_dates(datetime(2019, 3, 15, 20), 10)),
    (datetime(2019, 3, 15, 16, 12, 12), datetime(2019, 3, 15, 18), 12, tdf.generate_missing_dates(datetime(2019, 3, 15, 19), 9)),
    (datetime(2019, 3, 15, 18, 12, 12), None, 12, tdf.generate_missing_dates(datetime(2019, 3, 15, 17), 12))
])
def test_get_missing_default_plan_dates(current_time, last_datehour, max_default_plans, missing_dates):
    assert tdf.get_missing_default_plan_dates(current_time, last_datehour, max_default_plans) == missing_dates
