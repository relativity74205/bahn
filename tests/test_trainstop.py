import pytest

from TrainStop import TrainStop

import data.data_tests as data_tests


# TODO rewrite to check object and not dict
@pytest.mark.parametrize('test_input, expected_state', [
    (data_tests.event_ar, data_tests.event_ar_parsed),
    (data_tests.event_dp, data_tests.event_dp_parsed),
    (data_tests.event_ar_dp, data_tests.event_ar_dp_parsed)
])
def test_eval_default_plan(mocker, test_input, expected_state):
    ts = TrainStop(test_input, '8000086', 'Duisburg Hbf', '20190102', 12)
    actual_state = vars(ts)
    actual_state.pop('raw_event')
    actual_state.pop('raw_event_keys')
    actual_state.pop('_sa_instance_state')
    actual_state.pop('bahn_event_keys')
    assert actual_state == expected_state
