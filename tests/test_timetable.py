from datetime import datetime

import pytest

import config
from BahnAPI import BahnAPI
from Timetable import Timetable
from RequestsHeap import RequestsHeap

d1 = datetime(2019, 1, 1, 12, 0, 0)


@pytest.fixture
def tt():
    ba = BahnAPI()
    return Timetable(ba)


def test_timetable(tt):
    assert isinstance(tt.ba, BahnAPI)
    assert isinstance(tt.requests_heap, RequestsHeap)
    assert tt.bahnhof_dict == config.BAHNHOF_DICT


def test_get_changes(tt, mocker):
    eva_example = '8000085'
    station = 'Duisburg Hbf'
    request_type = 'full'
    current_time = datetime(2019, 1, 1, 12, 0, 0)
    raw_train_stop_changes = ['a', 'b', 'c']
    train_stop_changes = ['A', 'B', 'C']

    request_data_mocked = mocker.patch.object(tt, '_request_data')
    request_data_mocked.return_value = raw_train_stop_changes
    check_raw_train_stop_changes_mocked = mocker.patch.object(tt, '_check_raw_train_stop_changes')
    get_eva_mocked = mocker.patch.object(tt, '_get_eva')
    get_eva_mocked.return_value = eva_example
    process_raw_train_stop_changes_mocked = mocker.patch.object(tt, '_process_raw_train_stop_changes')
    process_raw_train_stop_changes_mocked.return_value = train_stop_changes

    actual = tt.get_changes(station, request_type, current_time)
    get_eva_mocked.assert_called_with(tt.bahnhof_dict, station)
    request_data_mocked.assert_called_with(request_type, station, current_time, eva_example)
    check_raw_train_stop_changes_mocked.assert_called_with(raw_train_stop_changes)
    process_raw_train_stop_changes_mocked.assert_called_with(raw_train_stop_changes, eva_example, station,
                                                             request_type, current_time)
    assert actual == train_stop_changes


# noinspection PyTypeChecker
class TestCheckRawTrainStopChanges:
    def test_good_case(self):
        raw_train_stop_changes = ({'a': 1}, {'a': 2})
        try:
            Timetable._check_raw_train_stop_changes(raw_train_stop_changes)
        except TypeError:
            pytest.fail('_check_raw_train_stop_changes threw exception though input should be good')

    def test_bad_case(self):
        raw_train_stop_changes = ({'a': 1}, 'b')
        with pytest.raises(TypeError):
            Timetable._check_raw_train_stop_changes(raw_train_stop_changes)


def test_get_default_timetable(tt, mocker):
    eva_example = '8000085'
    station = 'Duisburg Hbf'
    year = 2019
    month = 1
    day = 1
    hour = 1
    date = '20190101'
    hour_filled = '01'
    current_time = d1
    raw_train_stop_changes = ['a', 'b', 'c']
    train_stops = ['A', 'B', 'C']

    request_data_mocked = mocker.patch.object(tt, '_request_data')
    request_data_mocked.return_value = raw_train_stop_changes
    get_eva_mocked = mocker.patch.object(tt, '_get_eva')
    get_eva_mocked.return_value = eva_example
    get_date_mocked = mocker.patch.object(tt, '_get_date')
    get_date_mocked.return_value = date
    get_hour_mocked = mocker.patch.object(tt, '_get_hour')
    get_hour_mocked.return_value = hour_filled
    process_raw_train_stops_mocked = mocker.patch.object(tt, '_process_raw_train_stops')
    process_raw_train_stops_mocked.return_value = train_stops
    filter_train_stops_mocked = mocker.patch.object(tt, '_filter_train_stops')
    filter_train_stops_mocked.return_value = train_stops

    actual = tt.get_default_timetable(station, current_time, year, month, day, hour)
    get_eva_mocked.assert_called_with(tt.bahnhof_dict, station)
    request_data_mocked.assert_called_with('default', station, current_time, eva_example, date, hour_filled)
    process_raw_train_stops_mocked.assert_called_with(raw_train_stop_changes, eva_example, station,
                                                      date, hour)
    filter_train_stops_mocked.assert_called_with(train_stops, hour)

    assert actual == train_stops


@pytest.mark.parametrize('request_type', [
    'default',
    'full',
    'recent'
])
def test_request_data(tt, request_type, mocker):
    current_time = datetime(2019, 1, 1, 12, 0, 0)
    station = 'asd'
    eva = '8000085'
    date = '20190101'
    hour = '01'
    expected = ['a']

    wait_for_request_mocked = mocker.patch('requests_heap_functions.wait_for_available_requests')
    append_event_mocked = mocker.patch.object(tt.requests_heap, 'append_event')
    extract_raw_data_mocked = mocker.patch.object(tt, '_extract_raw_data')
    extract_raw_data_mocked.return_value = expected
    get_data_mocked = mocker.patch.object(tt.ba, 'get_data')
    get_data_mocked.return_value = {'a': 1}, 'asd'
    log_request_mocked = mocker.patch.object(tt, 'log_request')

    if request_type == 'default':
        actual = tt._request_data(request_type, station, current_time, eva, date, hour)
    elif request_type in ('full', 'recent'):
        actual = tt._request_data(request_type, station, current_time, eva)
    else:
        actual = None
    assert actual == expected

    wait_for_request_mocked.assert_called_with(tt.requests_heap)
    append_event_mocked.assert_called_with(current_time)
    extract_raw_data_mocked.assert_called_with({'a': 1})
    log_request_mocked.assert_called()

    if request_type == 'default':
        get_data_mocked.assert_called_with(request_type, eva, date, hour)
    elif request_type in ('full', 'recent'):
        get_data_mocked.assert_called_with(request_type, eva)


# noinspection PyTypeChecker
def test_base_case(mocker):
    def train_stop_change_mocked_side_effect(*args, **kwargs):
        return args[0]['key']

    eva_example = '8000085'
    station = 'Duisburg Hbf'
    request_type = 'a'
    raw_train_stop_changes = ({'key': 'a'}, {'key': 'b'})
    train_stop_change_mocked = mocker.patch('Timetable.TrainStopChange')
    train_stop_change_mocked.side_effect = train_stop_change_mocked_side_effect
    actual = Timetable._process_raw_train_stop_changes(raw_train_stop_changes, eva_example, station,
                                                       request_type, d1)
    assert len(train_stop_change_mocked.call_args_list) == 2
    assert train_stop_change_mocked.call_args_list[0][0][0] == raw_train_stop_changes[0]
    assert train_stop_change_mocked.call_args_list[1][0][0] == raw_train_stop_changes[1]
    assert train_stop_change_mocked.call_args_list[0][0][1] == eva_example
    assert train_stop_change_mocked.call_args_list[0][0][2] == station
    assert train_stop_change_mocked.call_args_list[0][0][3] == request_type
    assert train_stop_change_mocked.call_args_list[0][0][4] == d1
    assert actual == ('a', 'b')


# noinspection PyTypeChecker
def test_filter_train_stops(mocker):
    class TS:
        def __init__(self, mode: str, hour_int: int = None):
            if mode == 'none':
                self.planed_arrival_datetime = None
            elif mode == 'exact_hour':
                self.planed_arrival_datetime = datetime(2019, 1, 1, hour_int)
            else:
                self.planed_arrival_datetime = datetime(2019, 1, 1, hour_int + 1)

    train_stops_in = (TS('none'), TS('a', 2), TS('exact_hour', 2))
    train_stops_out = (train_stops_in[0], train_stops_in[2])

    assert train_stops_out == Timetable._filter_train_stops(train_stops_in, 2)


# noinspection PyTypeChecker
def test_process_raw_train_stops(mocker):
    def train_stops_mocked_side_effect(*args, **kwargs):
        return args[0]['key']

    eva_example = '8000085'
    station = 'Duisburg Hbf'
    date = '20190101'
    hour = 1
    raw_train_stops = ({'key': 'a'}, {'key': 'b'})
    train_stops_mocked = mocker.patch('Timetable.TrainStop')
    train_stops_mocked.side_effect = train_stops_mocked_side_effect
    actual = Timetable._process_raw_train_stops(raw_train_stops, eva_example, station,
                                                date, hour)
    assert len(train_stops_mocked.call_args_list) == 2
    assert train_stops_mocked.call_args_list[0][0][0] == raw_train_stops[0]
    assert train_stops_mocked.call_args_list[1][0][0] == raw_train_stops[1]
    assert train_stops_mocked.call_args_list[0][0][1] == eva_example
    assert train_stops_mocked.call_args_list[0][0][2] == station
    assert train_stops_mocked.call_args_list[0][0][3] == date
    assert train_stops_mocked.call_args_list[0][0][4] == hour
    assert actual == ('a', 'b')


# noinspection PyTypeChecker
class TestExtractRawData:
    def test_bad_type(self):
        with pytest.raises(ValueError):
            Timetable._extract_raw_data([])

    def test_none(self):
        with pytest.raises(ValueError):
            Timetable._extract_raw_data(None)

    def test_no_timetable(self):
        with pytest.raises(ValueError):
            Timetable._extract_raw_data({'a': 1})

    @pytest.mark.parametrize('raw_dict, raw_data_output', [
        ({'timetable': {'a': 1}}, ()),
        ({'timetable': {'s': [1]}}, (1,)),
        ({'timetable': {'s': [1], 'a': 1}}, (1,)),
        ({'timetable': {'s': 1}}, (1,)),
    ])
    def test_base_cases(self, raw_dict, raw_data_output):
        assert Timetable._extract_raw_data(raw_dict) == raw_data_output


class TestGetEva:
    bahnhof_dict = {'Duisburg Hbf': {'abbrev': 'EDG', 'eva': '42'},
                    'Essen Hbf': {'abbrev': 'EE', 'eva': '43'}}

    @pytest.mark.parametrize("station, eva", [
        ('Duisburg Hbf', '42'),
    ])
    def test_base_case(self, tt, station, eva):
        assert tt._get_eva(self.bahnhof_dict, station) == eva

    @pytest.mark.parametrize("station, eva", [
        ('', None),
        ('Oberhausen Hbf', None)
    ])
    def test_bad_cases(self, tt, station, eva):
        with pytest.raises(RuntimeError):
            tt._get_eva(self.bahnhof_dict, station)


@pytest.mark.parametrize("year, month, day, date", [
    (2019, 12, 25, '191225'),
    (2019, 1, 1, '190101'),
    (19, 1, 1, '190101'),
    (2001, 0, 0, '010000')
])
def test_get_date(year, month, day, date):
    assert Timetable._get_date(year, month, day) == date


@pytest.mark.parametrize("hour_int, hour_str", [
    (24, '24'),
    (9, '09'),
    (0, '00')
])
def test_get_hour(hour_int, hour_str):
    assert Timetable._get_hour(hour_int) == hour_str
