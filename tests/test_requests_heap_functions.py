from datetime import datetime, timedelta

import requests_heap_functions as rhf


def test_apirequest_wait_for_available_requests(requests_heap):
    current_time = datetime.now()

    requests_heap.append_event(current_time)
    requests_heap.append_event(current_time)
    requests_heap.append_event(current_time)
    rhf.wait_for_available_requests(requests_heap, 1)

    t1 = datetime.now()

    assert (t1 - current_time).total_seconds() >= 0.1


def test_apirequest_wait_for_available_requests_delay(requests_heap):
    current_time = datetime.now()
    delay = 0.15
    t0 = datetime.now()

    requests_heap.append_event(current_time)
    requests_heap.append_event(current_time)
    current_time = current_time + timedelta(seconds=delay)
    requests_heap.append_event(current_time)
    rhf.wait_for_available_requests(requests_heap, 3)

    t1 = datetime.now()

    assert (t1 - t0).total_seconds() >= delay + requests_heap.seconds_event_lifetime

