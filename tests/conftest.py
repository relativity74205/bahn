import sys
import os
from os.path import dirname, abspath, join
root_dir = join(dirname(dirname(abspath(__file__))), 'project')
sys.path.append(root_dir)
os.environ['BAHN_APP_MODE'] = 'test'
os.environ['BAHN_API_ACCESS_TOKEN'] = 'asd'
print(root_dir)


import pytest

import RequestsHeap
from DatabaseConnection import DatabaseConnection
from Control import Control


@pytest.fixture
def requests_heap():
    requests_heap = RequestsHeap.RequestsHeap()
    requests_heap.seconds_event_lifetime = 0.1
    requests_heap.limit_requests_heap = 3

    return requests_heap


@pytest.fixture
def db():
    db = DatabaseConnection()

    return db


@pytest.fixture
def control_mocked(mocker):
    _ = mocker.patch('control_functions.init_last_station_update')
    control = Control()

    return control
