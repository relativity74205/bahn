import time
from datetime import datetime


def test_requests_heap(requests_heap):
    assert len(requests_heap.heap) == 0
    assert requests_heap.seconds_event_lifetime
    assert requests_heap.limit_requests_heap


def test_requests_heap_append_event(requests_heap):
    current_time = datetime.now()
    requests_heap.append_event(current_time)
    assert len(requests_heap.heap) == 1
    assert (requests_heap.heap[0] - current_time).total_seconds() - requests_heap.seconds_event_lifetime == 0
    requests_heap.append_event(datetime.now())
    requests_heap.append_event(datetime.now())
    assert len(requests_heap.heap) == 3


def test_requests_heap_check_requests_heap(requests_heap):
    assert requests_heap.get_available_requests(datetime.now())
    for _ in range(requests_heap.limit_requests_heap):
        requests_heap.append_event(datetime.now())
    assert not requests_heap.get_available_requests(datetime.now())
    time.sleep(requests_heap.seconds_event_lifetime)
    assert requests_heap.get_available_requests(datetime.now())


def test_requests_heap_remove_expired_requests(requests_heap):
    requests_heap._remove_expired_requests(datetime.now())
    assert len(requests_heap.heap) == 0
    requests_heap.append_event(datetime.now())
    assert len(requests_heap.heap) == 1
    time.sleep(requests_heap.seconds_event_lifetime)
    requests_heap.append_event(datetime.now())
    requests_heap.append_event(datetime.now())
    assert len(requests_heap.heap) == 3

    time.sleep(requests_heap.seconds_event_lifetime / 10)
    requests_heap._remove_expired_requests(datetime.now())
    assert len(requests_heap.heap) == 2
    time.sleep(requests_heap.seconds_event_lifetime)
    requests_heap._remove_expired_requests(datetime.now())
    assert len(requests_heap.heap) == 0
