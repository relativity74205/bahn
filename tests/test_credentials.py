import os

from Credentials import Credentials


def test_credentials():
    c = Credentials()

    if c.mode == 'test':
        assert c.consumer_key == 'consumer_key_test'
        assert c.consumer_secret == 'consumer_secret_test'
        assert c.access_token == 'access_token_test'


def test_credentials_test():
    os.environ['BAHN_APP_MODE'] = 'prod'
    os.environ['BAHN_APP_CONSUMER_KEY'] = 'a'
    os.environ['BAHN_APP_CONSUMER_SECRET'] = 'b'
    os.environ['BAHN_APP_ACCESS_TOKEN'] = 'c'

    c = Credentials()
    assert c.consumer_key == 'a'
    assert c.consumer_secret == 'b'
    assert c.access_token == 'c'
