from project.BahnAPI import BahnAPI
ba = BahnAPI()
bahnhof_abbr = ba.get_bahnhof_abbrev('Duisburg Hbf')
print(bahnhof_abbr)

eva_number = ba.get_eva_number('Duisburg Hbf')
print(eva_number)
# import inspect
#
#
# class Test:
#     def __init__(self):
#         self.d = {'a': 'test'}
#         fun_test()
#
#
# def fun_test():
#     frame = inspect.stack()[1][0]# ['f_locals']['self']
#     #module = inspect.getclasstree(frame[0])
#     print(frame.f_locals['self'].d)
#     print('a')
#     a = 1
#
#
# t = Test()
