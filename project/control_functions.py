import time
import logging
from datetime import datetime
from typing import List, Dict, Any, Tuple

from DatabaseConnection import DatabaseConnection
import timetable_date_functions as tdf


def init_last_station_update(db, stations: List[str]) -> Dict[str, Any]:
    last_single_update = {}
    for station in stations:
        last_time_full = db.get_last_tstamp_request(station=station, request_type='full')
        last_time_recent = db.get_last_tstamp_request(station=station, request_type='recent')
        last_default_plan_date_hour = db.get_last_datehour_default_plan(station=station)
        last_datehour_datetime = tdf.datehour_to_datetime(last_default_plan_date_hour)

        last_single_update[station] = {'default': last_datehour_datetime,
                                       'full': last_time_full,
                                       'recent': last_time_recent}

    return last_single_update


def sleep(sleep_time: float):
    if sleep_time > 0:
        # logging.info(f'Sleeping for {sleep_time} seconds!')
        time.sleep(sleep_time)


def save_data(db: DatabaseConnection, objects: Tuple, obj_name: str):
    # TODO add save to file if db throws exception
    if objects is not None and len(objects) > 0:
        db.save_bulk(objects, obj_name)
    else:
        pass


def get_minutes_between_datetimes(d_later: datetime, d_earlier: datetime) -> float:
    return (d_later - d_earlier).total_seconds()


def get_next_update_type(current_time: datetime, last_station_update: Dict[str, datetime],
                         seconds_between_updates: Dict) -> (str, float):
    tstamp_last_update_default = last_station_update['default']
    tstamp_last_update_recent = last_station_update['recent']
    tstamp_last_update_full = last_station_update['full']

    if tstamp_last_update_default is None:
        return 'default', -1
    elif get_minutes_between_datetimes(tstamp_last_update_default, current_time) <= seconds_between_updates['default']:
        return 'default', -1
    elif tstamp_last_update_full is None:
        return 'full', -1
    elif get_minutes_between_datetimes(current_time, tstamp_last_update_full) >= seconds_between_updates['full']:
        return 'full', -1
    else:
        sleep_time = get_sleep_time(current_time, tstamp_last_update_recent, seconds_between_updates['recent'])
        if sleep_time > 0:
            return 'recent', sleep_time
        else:
            return 'recent', -1


def get_sleep_time(current_time: datetime, tstamp_last_update: datetime, recency_lifetime: int) -> float:
    delta = time_since_last_update(current_time, tstamp_last_update)
    if delta > 0:
        return recency_lifetime - delta
    else:
        return -1


def time_since_last_update(current_time: datetime, tstamp_last_update: datetime) -> float:
    if tstamp_last_update is not None:
        delta = (current_time - tstamp_last_update).total_seconds()
        return delta
    else:
        return -1


# TODO test
def last_updates_to_str(last_updates: Dict[str, datetime]) -> str:
    return ', '.join([f'{key}={tstamp}' for key, tstamp in last_updates.items()])
