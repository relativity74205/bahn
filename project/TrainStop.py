from typing import Dict
from datetime import datetime

from sqlalchemy import Column, String, DateTime

import config
from Base import Base
import train_stop_functions as tsf


class TrainStop(Base):
    __tablename__ = 'trainstops'

    trainstop_id = Column(String, primary_key=True)
    datehour_request = Column(String)
    station = Column(String)
    eva_number = Column(String)
    trip_type = Column(String)
    filter_flags = Column(String)
    owner = Column(String)
    train_type = Column(String)
    train_number = Column(String)
    line = Column(String)
    planed_arrival_datetime = Column(DateTime)
    planed_arrival_platform = Column(String)
    planed_arrival_from = Column(String)
    planed_departure_datetime = Column(DateTime)
    planed_departure_platform = Column(String)
    planed_departure_to = Column(String)

    def __init__(self, raw_event_data: Dict, eva: str, station: str, date: str, hour: int):
        # TODO log raw_event_data
        self.raw_event = raw_event_data
        self.raw_event_keys = tsf.get_raw_event_keys(self.raw_event)
        self.bahn_event_keys = config.BAHN_EVENT_KEYS
        self.datehour_request = tsf.get_datehour_request(date, hour)
        self.trainstop_id = tsf.get_id(self.bahn_event_keys, self.raw_event)
        self.station = station
        self.eva_number = eva
        self.trip_type = self.get_value_wrapper('trip_type')
        self.filter_flags = self.get_value_wrapper('filter_flags')
        self.owner = self.get_value_wrapper('owner')
        self.train_type = self.get_value_wrapper('train_type')
        self.train_number = self.get_value_wrapper('train_number')
        self.line = tsf.get_line(self.bahn_event_keys, self.raw_event, self.raw_event_keys,
                                 self.train_type, self.train_number)
        self._set_arrival_paras()
        self._set_departure_paras()

    def get_value_wrapper(self, key: str) -> str:
        return tsf.get_value(self.bahn_event_keys, self.raw_event, key)

    def _set_arrival_paras(self):
        if self.bahn_event_keys['arrival'] in self.raw_event_keys:
            self.planed_arrival_datetime = tsf.get_datetime(self.get_value_wrapper('arrival_datetime'),
                                                            self.raw_event)
            self.planed_arrival_platform = self.get_value_wrapper('arrival_platform')
            self.planed_arrival_from = tsf.get_departure_place(self.get_value_wrapper('arrival_path'),
                                                               self.raw_event)
        else:
            self.planed_arrival_from = self.station
            self.planed_arrival_platform = None
            self.planed_arrival_datetime = None

    def _set_departure_paras(self):
        if self.bahn_event_keys['departure'] in self.raw_event_keys:
            self.planed_departure_datetime = tsf.get_datetime(self.get_value_wrapper('departure_datetime'),
                                                              self.raw_event)
            self.planed_departure_platform = self.get_value_wrapper('departure_platform')
            self.planed_departure_to = tsf.get_destination_place(self.get_value_wrapper('departure_path'),
                                                                 self.raw_event)
        else:
            self.planed_departure_to = self.station
            self.planed_departure_platform = None
            self.planed_departure_datetime = None
