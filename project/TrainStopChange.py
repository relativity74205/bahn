from datetime import datetime
from typing import Dict

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey

import config
from Base import Base
import train_stop_functions as tsf


class TrainStopChange(Base):
    __tablename__ = 'trainstops_changes'

    change_id = Column(Integer, primary_key=True)
    station = Column(String)
    eva_number = Column(String)
    request_type = Column(String)
    tstamp_request = Column(DateTime)
    trainstop_id = Column(String, ForeignKey('trainstops.trainstop_id'))
    arrival_cancellation_time = Column(DateTime)
    changed_arrival_datetime = Column(DateTime)
    changed_arrival_platform = Column(String)
    changed_arrival_from = Column(String)
    changed_arrival_status = Column(String)
    departure_cancellation_time = Column(DateTime)
    changed_departure_datetime = Column(DateTime)
    changed_departure_platform = Column(String)
    changed_departure_to = Column(String)
    changed_departure_status = Column(String)

    def __init__(self, raw_event_data: Dict, eva: str, station: str, request_type: str, tstamp_request: datetime):
        # TODO log raw_event_data
        self.raw_event = raw_event_data
        self.raw_event_keys = tsf.get_raw_event_keys(self.raw_event)
        self.bahn_event_keys = config.BAHN_EVENT_KEYS

        self.trainstop_id = tsf.get_id(self.bahn_event_keys, self.raw_event)
        self.station = station
        self.eva_number = eva
        self.request_type = request_type
        self.tstamp_request = tstamp_request

        self._set_arrival_paras()
        self._set_departure_paras()

    def _get_value_wrapper(self, key: str) -> str:
        return tsf.get_value(self.bahn_event_keys, self.raw_event, key)

    def _set_arrival_paras(self):
        if self.bahn_event_keys['arrival'] in self.raw_event_keys:
            self.arrival_cancellation_time = tsf.get_datetime(self._get_value_wrapper('arrival_cancellation_time'),
                                                              self.raw_event)
            self.changed_arrival_datetime = tsf.get_datetime(self._get_value_wrapper('changed_arrival_datetime'),
                                                             self.raw_event)
            self.changed_arrival_platform = self._get_value_wrapper('changed_arrival_platform')
            self.changed_arrival_from = tsf.get_departure_place(self._get_value_wrapper('changed_arrival_path'),
                                                                self.raw_event)
            self.changed_arrival_status = self._get_value_wrapper('changed_arrival_status')

    def _set_departure_paras(self):
        if self.bahn_event_keys['departure'] in self.raw_event_keys:
            self.departure_cancellation_time = tsf.get_datetime(self._get_value_wrapper('departure_cancellation_time'),
                                                                self.raw_event)
            self.changed_departure_datetime = tsf.get_datetime(self._get_value_wrapper('changed_departure_datetime'),
                                                               self.raw_event)
            self.changed_departure_platform = self._get_value_wrapper('changed_departure_platform')
            self.changed_departure_to = tsf.get_destination_place(self._get_value_wrapper('changed_departure_path'),
                                                                  self.raw_event)
            self.changed_departure_status = self._get_value_wrapper('changed_departure_status')
