import os
import json
from typing import Dict


class Credentials:
    def __init__(self):
        self.mode = os.getenv('BAHN_APP_MODE', 'dev')
        self.token_type = 'Bearer'
        self.consumer_key = None
        self.consumer_secret = None
        self.access_token = None
        self.local_credential_file = '../config_credentials.json'
        self.set_credentials()

    def set_credentials(self):
        if self.mode == 'dev':
            credential_dict = self.get_credential_dict(self.local_credential_file)
            self.consumer_key = credential_dict['consumer_key']
            self.consumer_secret = credential_dict['consumer_secret']
            self.access_token = credential_dict['access_token']
        elif self.mode == 'test':
            self.consumer_key = 'consumer_key_test'
            self.consumer_secret = 'consumer_secret_test'
            self.access_token = 'access_token_test'
        elif self.mode == 'prod':
            self.consumer_key = os.getenv('BAHN_APP_CONSUMER_KEY')
            self.consumer_secret = os.getenv('BAHN_APP_CONSUMER_SECRET')
            self.access_token = os.getenv('BAHN_APP_ACCESS_TOKEN')

    @staticmethod
    def get_credential_dict(local_credential_file: str) -> Dict:
        with open(local_credential_file) as f:
            data = json.load(f)

        return data
