from typing import List, Tuple, Optional
from datetime import datetime, timedelta
import math


def get_missing_default_plan_dates(current_time: datetime, last_update: datetime, max_default_plans: int) \
        -> List[Tuple[int, int, int, int]]:
    if last_update is not None:
        time_delta = math.floor((last_update - current_time).total_seconds() / (60 * 60))
        amount_missing_datehours = max_default_plans - time_delta - 2
        next_default_plan_datetime = last_update + timedelta(hours=1)
    else:
        next_default_plan_datetime = current_time.replace(microsecond=0, second=0, minute=0) - timedelta(hours=1)
        amount_missing_datehours = max_default_plans

    missing_dates = generate_missing_dates(next_default_plan_datetime, amount_missing_datehours)
    return missing_dates


def generate_missing_dates(last_time: datetime, amount_missing_dates: int) -> List[Tuple[int, int, int, int]]:
    missing_dates = []
    for i in range(amount_missing_dates):
        default_plan_time = last_time + timedelta(hours=i + 1)
        missing_dates.append(default_plan_time)

    missing_dates_tuple = convert_missing_dates(missing_dates)

    return missing_dates_tuple


def convert_missing_dates(missing_dates: List[datetime]) -> List[Tuple[int, int, int, int]]:
    missing_dates = [(missing_date.year,
                      missing_date.month,
                      missing_date.day,
                      missing_date.hour) for missing_date in missing_dates]
    return missing_dates


def datehour_to_datetime(datehour: str) -> Optional[datetime]:
    try:
        return datetime.strptime(datehour, '%Y%m%d%H')
    except (TypeError, ValueError):
        return None


def datetime_to_datehour(dt: datetime) -> Optional[str]:
    try:
        return dt.strftime('%Y%m%d%H')
    except AttributeError:
        return None
