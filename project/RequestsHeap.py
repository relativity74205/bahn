from datetime import datetime, timedelta
import heapq

import config


class RequestsHeap:
    def __init__(self):
        self.heap = []
        self.seconds_event_lifetime = config.EVENT_LIFETIME_SECONDS
        self.limit_requests_heap = config.LIMIT_REQUESTS_HEAP

    def append_event(self, current_time: datetime):
        self.heap.append(current_time + timedelta(seconds=self.seconds_event_lifetime))

    def get_available_requests(self, current_time: datetime) -> int:
        self._remove_expired_requests(current_time)

        return self.limit_requests_heap - len(self.heap)

    def _remove_expired_requests(self, current_time: datetime):
        while True:
            if len(self.heap) == 0 or self.get_age_oldest_request(current_time) > 0:
                return
            else:
                heapq.heappop(self.heap)

    def get_age_oldest_request(self, current_time: datetime) -> int:
        return (self.heap[0] - current_time).total_seconds()
