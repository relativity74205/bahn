import logging
import os

import Control


def main():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    set_logging_config()

    logging.info('Start App; initializing APIRequests')
    control = Control.Control()
    logging.info('Start main loop!')
    control.main_loop()


def set_logging_config():
    logging.basicConfig(filename='../logs/bahn_scrapper_log.log',
                        format='%(asctime)s %(levelname)s: %(message)s',
                        level=logging.DEBUG)
    logging.getLogger("urllib3").setLevel(logging.CRITICAL)


if __name__ == '__main__':
    main()
