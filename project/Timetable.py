import json
from typing import Dict, Tuple
from datetime import datetime

import config
from BahnAPI import BahnAPI
from TrainStopChange import TrainStopChange
from TrainStop import TrainStop
from RequestsHeap import RequestsHeap
import requests_heap_functions as rhf


class Timetable:
    def __init__(self, bahn_api_object: BahnAPI):
        self.ba = bahn_api_object
        self.bahnhof_dict = config.BAHNHOF_DICT
        self.requests_heap = RequestsHeap()
        self.raw_data_path = config.RAW_DATA_PATH

    def get_changes(self, station: str, request_type: str, current_time: datetime) -> Tuple[TrainStopChange]:
        eva = self._get_eva(self.bahnhof_dict, station)

        raw_train_stop_changes = self._request_data(request_type, station, current_time, eva)

        self._check_raw_train_stop_changes(raw_train_stop_changes)
        train_stop_changes = self._process_raw_train_stop_changes(raw_train_stop_changes, eva, station,
                                                                  request_type, current_time)

        return train_stop_changes

    @staticmethod
    def _check_raw_train_stop_changes(raw_train_stop_changes: Tuple[Dict]) -> None:
        for raw_train_stop_change in raw_train_stop_changes:
            if not isinstance(raw_train_stop_change, dict):
                raise TypeError(f'{raw_train_stop_change=} is no dict, but of type {type(raw_train_stop_change)}.')

    def get_default_timetable(self, station: str, current_time: datetime,
                              year: int, month: int, day: int, hour: int) -> Tuple[TrainStop]:
        eva = self._get_eva(self.bahnhof_dict, station)
        date = self._get_date(year, month, day)
        hour_filled = self._get_hour(hour)

        # TODO retry 2 times if fail, add try-except block
        raw_train_stops = self._request_data('default', station, current_time, eva, date, hour_filled)

        train_stops = self._process_raw_train_stops(raw_train_stops, eva, station, date, hour)
        train_stops = self._filter_train_stops(train_stops, hour)

        return train_stops

    @staticmethod
    def _filter_train_stops(train_stops: Tuple[TrainStop], hour: int) -> Tuple[TrainStop]:
        return tuple([ts for ts in train_stops
                      if ts.planed_arrival_datetime is None or ts.planed_arrival_datetime.hour == hour])

    def _request_data(self, request_type: str, station: str, current_time: datetime, *args) -> Tuple:
        rhf.wait_for_available_requests(self.requests_heap)
        self.requests_heap.append_event(current_time)
        raw_dict, url = self.ba.get_data(request_type, *args)
        self.log_request(self.raw_data_path, request_type, station, current_time, raw_dict, url)

        raw_data = self._extract_raw_data(raw_dict)

        return raw_data

    @staticmethod
    def _process_raw_train_stop_changes(raw_train_stop_changes: Tuple[Dict], eva: str, station: str, request_type: str,
                                        tstamp_request: datetime) -> Tuple[TrainStopChange]:
        train_stop_changes = []
        for raw_train_stop_change in raw_train_stop_changes:
            train_stop_change = TrainStopChange(raw_train_stop_change, eva, station, request_type, tstamp_request)
            train_stop_changes.append(train_stop_change)

        return tuple(train_stop_changes)

    @staticmethod
    def _process_raw_train_stops(raw_train_stops: Tuple[Dict], eva: str, station: str, date: str, hour: int) \
            -> Tuple[TrainStop]:
        trainstops = []
        for raw_train_stop in raw_train_stops:
            ts = TrainStop(raw_train_stop, eva, station, date, hour)
            trainstops.append(ts)

        return tuple(trainstops)

    @staticmethod
    def _extract_raw_data(raw_dict: Dict) -> Tuple:
        if raw_dict is None:
            raise ValueError(f'{raw_dict=} is None')

        if not isinstance(raw_dict, Dict):
            raise ValueError(f'{raw_dict=} is not of type Dict')

        try:
            raw_data_temp = raw_dict['timetable']
        except KeyError as e:
            raise ValueError(f'Key "timetable" not found int {raw_dict=}') from e

        try:
            raw_data_list = raw_data_temp['s']
        except (KeyError, TypeError):
            raw_data_list = []

        if not isinstance(raw_data_list, list):
            raw_data_list = [raw_data_list]

        raw_data = tuple(raw_data_list)

        return raw_data

    @staticmethod
    def _get_hour(hour: int) -> str:
        return str(hour).zfill(2)

    @staticmethod
    def _get_date(year: int, month: int, day: int) -> str:
        # TODO check for valid date
        return str(year)[-2:].zfill(2) + str(month).zfill(2) + str(day).zfill(2)

    @staticmethod
    def _get_eva(bahnhof_dict: Dict[str, Dict], station: str) -> str:
        try:
            eva = bahnhof_dict[station]['eva']
        except KeyError as e:
            raise RuntimeError(f'{station=} not found in bahnhof_dict ') from e

        return eva

    @staticmethod
    def log_request(path, request_type: str, station: str, current_time: datetime, raw_dict: Dict, url: str):
        log_dict = {'url': url,
                    'raw_dict': raw_dict}
        file_name: str = f'{request_type}_{station}_{current_time.isoformat()}'
        for c in [' ', '(', ')', '/']:
            file_name = file_name.replace(c, '_')
        file_path = f'{path}/{file_name}.json'
        with open(file_path, 'w') as f:
            json.dump(log_dict, f, indent=4)
