import time
import logging
from datetime import datetime

from RequestsHeap import RequestsHeap


def wait_for_available_requests(rh: RequestsHeap, requests_needed: int = 1):
    while True:
        current_time = datetime.now()
        if rh.get_available_requests(current_time) < requests_needed:
            waittime = rh.get_age_oldest_request(current_time)
            logging.info(f'Waiting for {waittime} seconds')
            time.sleep(waittime)
        else:
            return
