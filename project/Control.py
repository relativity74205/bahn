import logging
from typing import Dict, Tuple
from datetime import datetime

import requests
from sqlalchemy.exc import SQLAlchemyError

import BahnAPI
import Timetable
import config
from DatabaseConnection import DatabaseConnection
from GlobalControl import GlobalControl
import control_functions as cf
import timetable_date_functions as tdf

db = DatabaseConnection()
ba = BahnAPI.BahnAPI()
GC = GlobalControl()


class Control:
    def __init__(self):
        self.timetable = Timetable.Timetable(ba)
        self.stations = config.BAHNHOF_DICT.keys()

        self.seconds_between_updates: Dict[str, int] = config.SECONDS_BETWEEN_UPDATES
        self.sleep_time_between_loops: int = config.SLEEP_TIME_BETWEEN_LOOPS
        self.max_default_plans: int = config.MAX_DEFAULT_PLANS

        self.last_station_updates: Dict[str, Dict[str, datetime]] = cf.init_last_station_update(db, self.stations)

    def main_loop(self):
        while GC.flag_running:
            self.main_inner_loop()

            logging.info(f'Sleeping for {self.sleep_time_between_loops} due to loop iteration end.')
            cf.sleep(sleep_time=self.sleep_time_between_loops)

    def main_inner_loop(self):
        for station in self.stations:
            last_updates: Dict[str, datetime] = self.last_station_updates[station]
            current_time: datetime = datetime.now()
            update_type, sleep_time = cf.get_next_update_type(current_time,
                                                              last_updates,
                                                              self.seconds_between_updates)
            if update_type == 'default':
                try:
                    last_datetime = self.get_default_tables(station, max_default_plans=self.max_default_plans,
                                                            current_time=current_time,
                                                            last_updates=last_updates)
                    last_updates['default'] = last_datetime
                except (ValueError, KeyError, TypeError, SQLAlchemyError, requests.exceptions.RequestException):
                    logging.exception(f'Exception during {station=} with {last_updates=} at {current_time=}, '
                                      f'chosen {update_type=}')
            elif update_type == 'full':
                try:
                    self.get_update(station, 'full', current_time, last_updates)
                    last_updates['full'] = current_time
                    last_updates['recent'] = current_time
                except (ValueError, KeyError, TypeError, SQLAlchemyError, requests.exceptions.RequestException):
                    logging.exception(f'Exception during {station=} with {last_updates=} at {current_time=}, '
                                      f'chosen {update_type=}')
            elif update_type == 'recent':
                cf.sleep(sleep_time)
                try:
                    self.get_update(station, 'recent', current_time, last_updates)
                    last_updates['recent'] = current_time
                except (ValueError, KeyError, TypeError, SQLAlchemyError, requests.exceptions.RequestException):
                    logging.exception(f'Exception during {station=} with {last_updates=} at {current_time=}, '
                                      f'chosen {update_type=}')
            cf.sleep(1)

    def get_default_tables(self, station: str, max_default_plans: int, current_time: datetime,
                           last_updates: Dict[str, datetime]) -> datetime:
        # logging.info(f'Starting getting default tables for {station=}, {current_time=}, {last_update=}')
        missing_datehours = tdf.get_missing_default_plan_dates(current_time, last_updates['default'], max_default_plans)

        for datehour in missing_datehours:
            self.get_default_table(station, current_time, datehour, last_updates)
            cf.sleep(1)
        # logging.info(f'Got all default tables and saved to db for {station=}.')
        last_datehour = missing_datehours[-1]
        last_datetime = datetime(year=int(last_datehour[0]), month=int(last_datehour[1]), day=int(last_datehour[2]),
                                 hour=int(last_datehour[3]))

        return last_datetime

    def get_default_table(self, station: str, current_time: datetime, datehour: Tuple,
                          last_updates: Dict[str, datetime]) -> None:
        logging.info(f'Getting default tables for {station}; {datehour=}, '
                     f'last_updates={cf.last_updates_to_str(last_updates)}')
        train_stops: Tuple = self.timetable.get_default_timetable(station, current_time, *datehour)

        cf.save_data(db, train_stops, 'TrainStop')
        # logging.info(f'Saved to DB successfully {len(train_stops)} default table for {station=}, {datehour=}')

    def get_update(self, station: str, update_type: str, current_time: datetime, last_updates: Dict[str, datetime]):
        logging.info(f'Getting {update_type} update for {station}, last_updates={cf.last_updates_to_str(last_updates)}')
        train_stop_changes: Tuple = self.timetable.get_changes(station, update_type, current_time)

        cf.save_data(db, train_stop_changes, 'TrainStopChange')
        # logging.info(f'Saved to DB successfully {len(train_stop_changes)} updates for {station=}, {update_type=}.')
