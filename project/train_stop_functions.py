import logging
from datetime import datetime
from typing import Dict, Optional, List


def get_value(bahn_event_keys: Dict, raw_event: Dict, key: str) -> Optional[str]:
    try:
        keys = bahn_event_keys[key]
        return raw_event[keys['para1']][keys['para2']]
    except (KeyError, TypeError):
        return None


def get_id(bahn_event_keys: Dict, raw_event: Dict) -> Optional[str]:
    try:
        return raw_event[bahn_event_keys['id']]
    except (KeyError, TypeError):
        logging.critical(f'Could not extract id from {raw_event=}', exc_info=True)
        return None


def get_raw_event_keys(raw_event: Dict) -> List[str]:
    try:
        return list(raw_event.keys())
    except AttributeError as e:
        msg = f'Unexpected error (raw_event is not a list), aborting. {raw_event=}'
        raise AttributeError(msg) from e


def get_datetime(time_str: str, raw_event: Dict) -> Optional[datetime]:
    try:
        return datetime.strptime(time_str, '%y%m%d%H%M')
    except (TypeError, ValueError):
        return None


def get_departure_place(ppth: str, raw_event: Dict) -> Optional[str]:
    if ppth == '':
        return None

    try:
        return ppth.split('|')[0]
    except (IndexError, AttributeError):
        return None


def get_destination_place(ppth: str, raw_event: Dict) -> Optional[str]:
    if ppth == '':
        return None

    try:
        return ppth.split('|')[-1]
    except (IndexError, AttributeError):
        return None


def get_datehour_request(date: str, hour: int) -> str:
    return date + str(hour).zfill(2)


def get_line(bahn_event_keys: Dict, raw_event: Dict, raw_event_keys: List, train_type: str, train_number: str) \
        -> Optional[str]:
    # TODO add exception handling
    ar = bahn_event_keys['arrival']
    dp = bahn_event_keys['departure']
    line = bahn_event_keys['line']
    if ar in raw_event_keys and line in raw_event[ar].keys():
        line = str(get_value(bahn_event_keys, raw_event, 'arrival_line'))
    elif dp in raw_event_keys and line in raw_event[dp].keys():
        line = str(get_value(bahn_event_keys, raw_event, 'departure_line'))
    else:
        line = str(train_type) + str(train_number)

    if line.isdigit():
        return train_type + str(line)
    else:
        return line
