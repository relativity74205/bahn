#!/bin/bash

sudo apt-get -y update
sudo apt-get -y install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget libsqlite3-dev
wget -P ~ "https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tgz"
tar xzf ~/Python-3.8.1.tgz -C ~
cd ~/Python-3.8.1/
#sudo ./configure --enable-optimizations --enable-loadable-sqlite-extensions --prefix=/home/ubuntu/python/python3.8.1 && sudo make && sudo make install
./configure --enable-loadable-sqlite-extensions --prefix=/home/ubuntu/python/python3.8.1
make
make altinstall
cd ~
sudo rm ~/Python-3.8.1.tgz
sudo rm -rf ~/Python-3.8.1/
mkdir bahnscrapper